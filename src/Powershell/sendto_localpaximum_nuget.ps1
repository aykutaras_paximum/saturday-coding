$nugetPackName = "NUSPEC_FILE";
$localPaximumPath = "PATH_TO_REMOTE";

Write-Host "Building solution"
msbuild ..\Paximum.Agents.sln /t:Clean /t:Build

Write-Host "Packing nuspec"
nuget pack $nugetPackName;

$nugetPackage = Get-ChildItem -name *.nupkg;
Write-Host $nugetPackage" created";

if (!(Test-Path "$localPaximumPath\$nugetPackage")) {
	Write-Host "Moving file to $localPaximumPath";
	mv $nugetPackage "$localPaximumPath\$nugetPackage";
} else {
  Write-Host "$nugetPackage already exists in $localPaximumPath";
	Write-Host "Removing file from local directory";
	rm $nugetPackage;
}