$pageant = "pageant.exe";
$sshKey =  [environment]::getfolderpath("user") + "\.ssh\default_rsa.ppk";
$selfScript = ".\selectpullandupdate.ps1";

if (!$args) {
	& $pageant $sshKey -c powershell -NoProfile -ExecutionPolicy unrestricted -File $selfScript "$True"
	return;
}

$repositories = Get-ChildItem -name -directory;

do {
	$message = "Select a folder to pull and update:" + [Environment]::NewLine;

	$message += "(0) All repositories" + [Environment]::NewLine;
	$i = 1;
	$repositories | ForEach-Object {
		$message += "($i) $_" + [Environment]::NewLine;
		$i++;
	}
	$message += "(CTRL+C) Exit" + [Environment]::NewLine;
	$message += "Repository";

	$repoInput = Read-Host $message;
	$repoIndex = [int]$repoInput;

	if (($repoIndex -gt 0) -and ($repoIndex -le $repositories.Count)) {
		$repo = $repositories[$repoIndex - 1];
		
		if (!(Test-Path -Path $repo)) {
			echo "Invalid path"
		  continue;
		}

		if (!(Test-Path -Path "$repo/.hg")) {
			echo "$repo is not a mercurial repository";
			echo "";
			continue;
		}

		cd $repo;
		echo "Pulling $repo repository";
		hg pull;
		echo "Updating $repo repository";
		hg up;
		echo "Build $repo solution";
		msbuild /t:Clean /t:Rebuild /noconsolelogger;
		cd ..;
	}

	if ($repoIndex -eq 0) {
		$repositories | ForEach-Object {
			if (!(Test-Path -Path $_)) {
				echo "Invalid path"
				return;
			}

			if (!(Test-Path -Path "$_/.hg")) {
				echo "$_ is not a mercurial repository";
				echo "";
				return;
			}

			cd $_;
			echo "Pulling $_ repository";
			hg pull;
			echo "Updating $_ repository";
			hg up;
			echo "Build $_ solution";
			msbuild /t:Clean /t:Rebuild /noconsolelogger;
			cd ..;
		}
	}
} while(1 -eq 1)