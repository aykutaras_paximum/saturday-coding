function Add($first, $second) {
	$ans = $first + $second
	
	foreach ($i in $Input) {
		$ans = $ans + $i
	}
	
	return $ans
}

function Multiply {
	$ans = $args[0] * $args[1]
	
	foreach ($i in $Input) {
		$ans = $ans * $i
	}
	
	return $ans
}

function Test-Output {
  Write-Output "Hello World"
}

function Test-Output2 {
  Write-Host "Hello World" -foreground Green
}

filter Receive-Output {
	Write-Host "$_" -foreground Yellow
}

#Output piped to another function, not displayed in first.
Test-Output | Receive-Output

#Output not piped to 2nd function, only displayed in first.
Test-Output2 | Receive-Output 

#Pipeline sends to Out-Default at the end.
Test-Output

Multiply 2 3 | Add 1 2 | Receive-Output
Add 1 2 | Multiply 2 3 | Receive-Output
