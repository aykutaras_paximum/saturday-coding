# Alias for 7-zip 
if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe" 
 
$directoryPath = "C:\willbezipped"
$directoryName = Split-Path $directoryPath -leaf
sz a -t7z "$directoryPath\directoryName.7z" "$directoryPath"