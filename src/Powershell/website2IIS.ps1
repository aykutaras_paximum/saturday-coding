param (
		[switch]$publish = $false,
    [switch]$install = $false,
		[switch]$update = $false,
		[switch]$delete = $false
 )
 
 $wwwRootPath = "c:\inetpub\wwwroot\demo";
 $webAppPath = "C:\Projects\WebApplication1"
 if ($publish) {
	msbuild $webAppPath"\WebApplication1\WebApplication1.csproj" /t:WebPublish /p:PublishProfile="FolderPublish"
 }
 
 if ($install) {
  Write-Host "Creating demo_pool"
	New-WebAppPool demo_pool
	$pool = Get-Item "IIS:\AppPools\demo_pool"
	$pool.managedRuntimeVersion = "v4.0"
	$pool.processModel.identityType = 2 #NetworkService
	$pool | Set-Item
	if (-not (Test-Path $wwwRootPath)) {
		Write-Host "Demo path is not available creating it"
		mkdir $wwwRootPath
	}
	cp $webAppPath"\Published\*" $wwwRootPath -Force -Recurse
	New-Website -Name "demo" -PhysicalPath $wwwRootPath -ApplicationPool "demo_pool" -Port 8686
	
	Write-Host "removing publish folder"
	rm $webAppPath"\Published" -Recurse -Force
	Write-Host "try http://localhost:8686/"
	
} elseif ($update) {
	$webSite = Get-Item "IIS:\Sites\demo"
	$filesToDelete = $webSite.physicalPath + "\*"

	if ((Get-WebsiteState -Name "demo").Value -ne "Stopped") {
		$webSite.Stop()
	}
	$poolName = $webSite.applicationPool
	$pool = Get-Item "IIS:\AppPools\$poolName"

	if ((Get-WebAppPoolState -Name $poolName).Value -ne "Stopped") {
		$pool.Stop()
	}

	rm $filesToDelete -Recurse -Force
	cp $webAppPath"\Published\*" $webSite.physicalPath -Force -Recurse

	$pool.Start()
	$webSite.Start()
} elseif ($delete) {
	$webSite = Get-Item "IIS:\Sites\demo"
	if ((Get-WebsiteState -Name "demo").Value -ne "Stopped") {
		$webSite.Stop()
	}
	$poolName = $webSite.applicationPool
	$pool = Get-Item "IIS:\AppPools\$poolName"

	if ((Get-WebAppPoolState -Name $poolName).Value -ne "Stopped") {
		$pool.Stop()
	}
	Remove-Website -Name "demo"

	Remove-WebAppPool -Name $poolName

	rm $webSite.physicalPath -Recurse -Force
}