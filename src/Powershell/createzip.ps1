$directoryPath = Read-Host 'Select folder to zip? '

if($directoryPath -ne ""){
	$fileZipName = $directoryPath + ".zip"	

	if(([System.IO.FileInfo]$fileZipName).Exists) {
		"`r`n remove " + $fileZipName
		remove-item ($fileZipName)
	}

	$ionicZipDll=(Get-ChildItem -filter Ionic.Zip.dll).FullName;
	[System.Reflection.Assembly]::LoadFrom($ionicZipDll)
	$fileZip = new-object Ionic.Zip.ZipFile
	$fileZip.Encryption = [Ionic.Zip.EncryptionAlgorithm]::WinZipAes256
	$fileZip.Password = "xxxxx"
	$fileZip.AddDirectory($directoryPath)

	"`r`n Please wait `r`n  Loading..."

	$fileZip.Save($fileZipName)
	$fileZip.Dispose()

}
else { write-error "We have an error! Please select"+$strModule;  sleep 5}