﻿namespace ThermalHotel
{
    public class StandartRoom : IThermalHotel
    {
        private readonly IThermalHotel _thermalHotel;

        public StandartRoom(IThermalHotel thermalHotel)
        {
            _thermalHotel = thermalHotel;
        }

        public double GetPrice()
        {
            return _thermalHotel.GetPrice();
        }

        public string GetDescription()
        {
            return "Standart Room " + _thermalHotel.GetDescription();
        }
    }
}