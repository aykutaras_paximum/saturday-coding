﻿namespace ThermalHotel
{
    public class FamilyRoom : IThermalHotel
    {
        private readonly IThermalHotel _thermalHotel;

        public FamilyRoom():this(new EmptyHotel())
        {

        }

        public FamilyRoom(IThermalHotel thermalHotel)
        {
            _thermalHotel = thermalHotel;
        }

        public double GetPrice()
        {
            return _thermalHotel.GetPrice()+30;
        }

        public string GetDescription()
        {
            return "Family Room " + _thermalHotel.GetDescription();
        }
    }
}