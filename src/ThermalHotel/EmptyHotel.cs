﻿using System;

namespace ThermalHotel
{
    public class EmptyHotel:IThermalHotel
    {
        public double GetPrice()
        {
            return 0;
        }

        public string GetDescription()
        {
            return String.Empty;
        }
    }
}