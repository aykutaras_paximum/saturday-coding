﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiSampleUnitTests.Messages
{
    public class HotelRequest
    {
        public string Location { get; set; }
    }
}
