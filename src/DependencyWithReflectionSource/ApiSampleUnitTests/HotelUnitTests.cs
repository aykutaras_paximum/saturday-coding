﻿using ApiSampleUnitTests.Messages;
using NUnit.Framework;
using FluentAssertions;
using RestSharp;

namespace ApiSampleUnitTests
{
    [TestFixture]
    public class HotelUnitTests
    {
        private const string Server = "http://localhost:9999/api";

        [Test]
        public void Get_Hotel()
        {
            var client = new RestClient(Server);
            var request = new RestRequest("hotel?Location=Antalya", Method.GET)
            {
                RequestFormat = DataFormat.Json
            };

            var response = client.Execute<HotelResponse>(request);
            var responseData = response.Data;

            response.ContentType.Should().Be("application/json; charset=utf-8");

            responseData.Id.Should().Be(5);
            responseData.Name.Should().Be("Falcon");
            responseData.Location.Should().Be("Antalya");
        }

        [Test]
        public void Post_Hotel()
        {
            var client = new RestClient(Server);
            var request = new RestRequest("hotel", Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            var model = new HotelRequest
            {
                Location = "Antalya"
            };

            request.AddBody(model);

            var response = client.Execute<HotelResponse>(request);
            var responseData = response.Data;

            response.ContentType.Should().Be("application/json; charset=utf-8");

            responseData.Id.Should().Be(5);
            responseData.Name.Should().Be("Falcon");
            responseData.Location.Should().Be(model.Location);
        }
    }
}
