﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleMessagingModel
{
    public class Response
    {
        public Response()
        {
            Error = new Error { Code = string.Empty, Message = string.Empty };
            IsSuccessful = true;
        }

        public Error Error { get; set; }
        public bool IsSuccessful { get; set; }
    }
}
