﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleMessagingModel
{
    public interface IServiceHandler<in TRequest, out TResponse>
        where TRequest: IRequest<TResponse>
    {
        TResponse Retrieve(TRequest request);
    }
}
