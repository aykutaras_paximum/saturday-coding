﻿using SampleHotelDll.Messages;
using SampleMessagingModel;

namespace SampleHotelDll
{
    public class HotelMessageHandler : IServiceHandler<HotelRequest, HotelResponse>
    {
        public HotelResponse Retrieve(HotelRequest request)
        {
            var response = new HotelResponse { Id = 5, Name = "Falcon", Location = request.Location };
            return response;
        }
    }
}
