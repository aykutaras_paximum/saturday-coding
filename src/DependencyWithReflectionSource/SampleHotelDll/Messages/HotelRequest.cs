﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessagingModel;

namespace SampleHotelDll.Messages
{
    [MessageRoute("hotel")]
    public class HotelRequest : IRequest<HotelResponse>
    {
        public string Location { get; set; }
    }
}
