﻿using System;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;

namespace SampleWebApi.Extensions
{
    public static class HttpRequestMessageExtension
    {
        public static dynamic DeserializeRequestMessage(this HttpRequestMessage request, Type apiRequestType)
        {
            if (request.Method != HttpMethod.Get)
            {
                return request.Content.ReadAsAsync(apiRequestType).Result;
            }

            var parameters = request.GetQueryNameValuePairs().ToDictionary(x => x.Key, y => y.Value);
            var jsonStr = JsonConvert.SerializeObject(parameters);
            return JsonConvert.DeserializeObject(jsonStr, apiRequestType);
        }
    }
}