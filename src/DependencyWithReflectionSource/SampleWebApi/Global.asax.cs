﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using SampleWebApi.Windsor;

namespace SampleWebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private readonly WindsorContainer container;
        public WebApiApplication()
        {
            container = new WindsorContainer();
        }

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(RegisterDependencyResolver);
            InstallDependencies();
        }

		private void RegisterDependencyResolver(HttpConfiguration config)
		{
			config.DependencyResolver = new WindsorDependencyResolver(this.container.Kernel);
		}

		private void InstallDependencies()
		{
			this.container.Install(FromAssembly.InDirectory(new AssemblyFilter(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\bin"))));
		}
    }
}
