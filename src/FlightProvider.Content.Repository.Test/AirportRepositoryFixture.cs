﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using FakeItEasy;
using FlightProvider.Content.Repository.Interface;
using FlightProvider.Content.Repository.Model;
using FluentAssertions;
using NUnit.Framework;

namespace FlightProvider.Content.Repository.Test
{
    [TestFixture]
    public class AirportRepositoryFixture
    {
        private IAirportRepository airportRepository;

        [TestFixtureSetUp]
        public void Setup()
        {
            airportRepository = IoCUtil.Resolve<IAirportRepository>();            
            SaveAirport();
        }

       // [Test]
        public void SaveAirport()
        {
            var airports = GetAirports();
            foreach (var airport in airports.Airports)
            {
                // airport.Code.Length.Should().BeLessOrEqualTo(3);
                if (airport.Code.Length <= 3)
                {
                    if (airportRepository.GetByCode(airport.Code) == null)
                        airportRepository.Insert(airport);
                    else
                        airportRepository.Update(airport);
                }
            }

        }

        [Test]
        public void Performance_of_LikeNameMethod()
        {
          
            Stopwatch watch=new Stopwatch();
            watch.Start();
            string[] searchKeys = new string[10] {"Ant", "Izm", "Istan", "Alaba", "A", "Bu", "Sam", "Sor", "D", "Kes"};
            foreach (var searchKey in searchKeys)
            {
                var airports = airportRepository.GetAirpotyLikeName(searchKey);
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedTicks);

        }

        private AirportResult GetAirports()
        {
            string path = @"..\..\Airports.xml";
            var serializer = new XmlSerializer(typeof(AirportResult));
            var reader = new StreamReader(path);
            var airports = (AirportResult)serializer.Deserialize(reader);
            reader.Close();
            return airports;
        }
    }
}
