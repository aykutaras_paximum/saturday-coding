* 21.12.2013 - Builder Pattern and Fluent Interface - by Vitaliy Tsvayer
* 28.12.2013 - Dependency Injection Pattern - by Koray Uysal
* 04.01.2014 - Decorator Pattern - by Haluk Yazici
* 11.01.2014 - Introduction to DVCSs like Git/Mercurial and best practices - by Serol Güzel
* 18.01.2014 - Practical TDD with the help of Resharper/NCrunch/etc - by Vitaliy Tsvayer
* 25.01.2014 - Intriduction to SignalR - Realtime Web Application - by Batuhan Kalkan
* 01.02.2014 - Applying Git/Mercurial - by Serol Güzel
* 08.02.2014 - Infrastructure as a Code : Amazon AWS - by Vitaliy Tsvayer
* 08.03.2014 - Connecting independent projects with reflection by Aykut Aras - Haluk Yazıcı